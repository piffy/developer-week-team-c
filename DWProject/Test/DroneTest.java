import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class DroneTest {

    @Test
    public void testGetPositionX() {
        // Arrange
        Drone drone = new Drone(10, 20);

        // Act
        int PositionX = drone.getPositionX();

        // Assert
        assertEquals(10, PositionX);
    }

    @Test
    public void testGetPositionY() {
        // Arrange
        Drone drone = new Drone(10, 20);

        // Act
        int PositionY = drone.getPositionY();

        // Assert
        assertEquals(20, PositionY);
    }

    @Test
    public void testSetPositionX() {
        // Arrange
        Drone drone = new Drone(10, 20);

        // Act
        drone.setPositionX(30);

        // Assert
        assertEquals(30, drone.getPositionX());
    }

    @Test
    public void testSetPositionY() {
        // Arrange
        Drone drone = new Drone(10, 20);

        // Act
        drone.setPositionY(40);

        // Assert
        assertEquals(40, drone.getPositionY());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInvalidPositionXConstructor() {
        // Arrange & Act
        Drone drone = new Drone(-5, 20); // Should throw an IllegalArgumentException
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInvalidPositionYConstructor() {
        // Arrange & Act
        Drone drone = new Drone(10, -15); // Should throw an IllegalArgumentException
    }
}