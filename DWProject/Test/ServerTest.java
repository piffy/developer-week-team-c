import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import java.io.IOException;

class ServerTest {

    @Test
    void initTest() throws IOException {
        Server s = new Server("");
        assertEquals("NullaFacenti", s.team);
        assertNotNull(s.seed);
        assertNotNull(s.width);
        assertNotNull(s.height);
        assertNotNull(s.positionX);
        assertNotNull(s.positionY);
        assertNotNull(s.energy);
    }

    @Test
    void lookTest() throws IOException {
        Server s = new Server("");
        s.look();
        assertEquals("NullaFacenti", s.team);
        assertNotNull(s.seed);
        assertNotNull(s.neighbors);
        assertNotNull(s.positionX);
        assertNotNull(s.positionY);
        assertNotNull(s.energy);
    }

    @Test
    void moveTest() throws IOException {
        Server s = new Server("");
        s.move(2);
        assertEquals("NullaFacenti", s.team);
        assertNotNull(s.seed);
        assertNotNull(s.positionX);
        assertNotNull(s.positionY);
        assertNotNull(s.energy);
    }

    @Test
    void loadTest() throws IOException {
        Server s = new Server("");
        s.load();
        assertEquals("NullaFacenti", s.team);
        assertNull(s.inventory);
        assertNotNull(s.energy);
    }

    @Test
    void unloadTest() throws IOException {
        Server s = new Server("");
        s.unload();
        assertEquals("NullaFacenti", s.team);
        assertNull(s.status);
        assertNotNull(s.energy);
    }
}