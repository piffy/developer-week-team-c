import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CoordinateTest {

    @Test
    public void testGetX() {
        // Arrange
        Coordinate coordinate = new Coordinate(3, 5);

        // Act
        int x = coordinate.getX();

        // Assert
        assertEquals(3, x);
    }

    @Test
    public void testGetY() {
        // Arrange
        Coordinate coordinate = new Coordinate(3, 5);

        // Act
        int y = coordinate.getY();

        // Assert
        assertEquals(5, y);
    }

    @Test
    public void testConstructorAndGetters() {
        // Arrange
        Coordinate coordinate = new Coordinate(8, 12);

        // Act
        int x = coordinate.getX();
        int y = coordinate.getY();

        // Assert
        assertEquals(8, x);
        assertEquals(12, y);
    }
}
