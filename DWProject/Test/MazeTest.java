import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MazeTest {

    @Test
    public void testInitializeMatrix() {
        // Arrange
        Maze maze = new Maze(3, 4);

        // Act
        int[][] matrix = maze.getMatrix();

        // Assert
        assertEquals(3, matrix.length);
        assertEquals(4, matrix[0].length);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                assertEquals(1, matrix[i][j]);
            }
        }
    }

    @Test
    public void testSetWall() {
        // Arrange
        Maze maze = new Maze(3, 4);

        // Act
        maze.setWall(1, 2);
        int[][] matrix = maze.getMatrix();

        // Assert
        assertEquals(1, matrix[2][1]); // Walls are marked as 1
    }

    @Test
    public void testSetHallway() {
        // Arrange
        Maze maze = new Maze(3, 4);

        // Act
        maze.setHallway(1, 2);
        int[][] matrix = maze.getMatrix();

        // Assert
        assertEquals(0, matrix[2][1]); // Hallways are marked as 0
    }

    @Test
    public void testSetCup() {
        // Arrange
        Maze maze = new Maze(3, 4);

        // Act
        maze.setCup(1, 2);
        int[][] matrix = maze.getMatrix();

        // Assert
        assertEquals(3, matrix[2][1]); // Cups are marked as 3
    }

    @Test
    public void testSetStart() {
        // Arrange
        Maze maze = new Maze(3, 4);

        // Act
        maze.setStart(1, 2);
        int[][] matrix = maze.getMatrix();

        // Assert
        assertEquals(2, matrix[2][1]); // Start position is marked as 2
    }

    @Test
    public void testSetStartX() {
        // Arrange
        Maze maze = new Maze(3, 4);

        // Act
        maze.setStartX(1);

        // Assert
        assertEquals(1, maze.getStartX());
    }

    @Test
    public void testSetStartY() {
        // Arrange
        Maze maze = new Maze(3, 4);

        // Act
        maze.setStartY(2);

        // Assert
        assertEquals(2, maze.getStartY());
    }
}
