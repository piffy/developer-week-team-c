public class Maze {
    private int matrix[][];
    private int startX;
    private int startY;

    public Maze(int width, int height) {
        matrix = new int[width][height];
        for(int i=0; i<width; i++){
            for(int j=0; j<height; j++){
                matrix[i][j] = 1;
            }
        }
    }
    public  void setWall (int positionX, int positionY){
        this.matrix[positionY][positionX] = 1;
    }
    public  void setHallway (int positionX, int positionY){
        this.matrix[positionY][positionX] = 0;
    }
    public void setCup(int positionX, int positionY){
        this.matrix[positionY][positionX] = 3;
    }
    public void setStart(int positionX, int positionY){
        this.matrix[positionY][positionX] = 2;
    }

    public int getStartX() {
        return startX;
    }
    public int getStartY() {
        return startY;
    }
    public void setStartX(int startX) {
        this.startX = startX;
    }
    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int[][] getMatrix(){
        return matrix;
    }
}