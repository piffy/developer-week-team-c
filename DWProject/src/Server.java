import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.*;

public class Server {
    public String team;
    public int energy;
    public String seed;
    public int positionX;
    public int positionY;
    public int width;
    public int height;
    public String inventory[];
    public int neighbors[];
    public String status;

    public Server(String seed) throws IOException {
        this.team = "NullaFacenti";
        this.seed = seed;
        init();
    }

    public void init() throws IOException {
        JSONObject messageJson = new JSONObject();
        URL url = new URL("https://dw.gnet.it/init");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        messageJson.put("team", team);
        messageJson.put("seed", seed);

        String messageStr = messageJson.toString();

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = messageStr.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                response.append(line);
            }
            String responseStr = response.toString();
            JSONObject result = new JSONObject(responseStr);
            team = result.getString("team");
            seed = result.getString("seed");
            width = result.getInt("width");
            height = result.getInt("height");
            positionX = result.getInt("posx");
            positionY = result.getInt("posy");
            energy = result.getInt("Energy");

            isr.close();
            br.close();
        }
    }

    public void look() throws IOException{
        JSONObject messageJson = new JSONObject();
        URL url = new URL("https://dw.gnet.it/look");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        messageJson.put("team", team);
        messageJson.put("seed", seed);

        String messageStr = messageJson.toString();

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = messageStr.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                response.append(line);
            }
            String responseStr = response.toString();
            JSONObject result = new JSONObject(responseStr);
            JSONArray neighbor = result.getJSONArray("neighbors");
            this.neighbors = new int[neighbor.length()];
            for(int i=0; i<neighbor.length(); i++){
                this.neighbors[i] = neighbor.getInt(i);
            }
            positionX = result.getInt("posx");
            positionY = result.getInt("posy");
            energy = result.getInt("Energy");

            isr.close();
            br.close();
        }
    }

    public void move(int direction) throws IOException{
        JSONObject messageJson = new JSONObject();
        URL url = new URL("https://dw.gnet.it/move");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        messageJson.put("team", team);
        messageJson.put("seed", seed);
        messageJson.put("move", direction);

        String messageStr = messageJson.toString();

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = messageStr.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                response.append(line);
            }
            String responseStr = response.toString();
            JSONObject result = new JSONObject(responseStr);
            positionX = result.getInt("posx");
            positionY = result.getInt("posy");
            energy = result.getInt("Energy");

            isr.close();
            br.close();
        }
    }

    public void load() throws IOException{
        JSONObject messageJson = new JSONObject();
        URL url = new URL("https://dw.gnet.it/load");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        messageJson.put("team", team);
        messageJson.put("seed", seed);

        String messageStr = messageJson.toString();

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = messageStr.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                response.append(line);
            }
            String responseStr = response.toString();
            JSONObject result = new JSONObject(responseStr);
            JSONArray inv = result.getJSONArray("inventory");
            inventory = new String[inv.length()];
            for(int i=0; i<inv.length(); i++){
                inventory[i] = inv.getString(i);
            }
            energy = result.getInt("Energy");

            isr.close();
            br.close();
        }
    }

    public void unload() throws IOException{
        JSONObject messageJson = new JSONObject();
        URL url = new URL("https://dw.gnet.it/unload");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        messageJson.put("team", team);
        messageJson.put("seed", seed);

        String messageStr = messageJson.toString();

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = messageStr.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                response.append(line);
            }
            String responseStr = response.toString();
            JSONObject result = new JSONObject(responseStr);
            status = result.getString("status");
            energy = result.getInt("Energy");

            isr.close();
            br.close();
        }
    }
}