public class Drone {
    private int PositionX;
    private int PositionY;

    public Drone(int X,int Y) {
        if(X>=0){this.PositionX=X;}
        else {throw new IllegalArgumentException("Start invalid for coordinate negative");}
        if(Y>=0){this.PositionY=Y;}
        else {throw new IllegalArgumentException("Start invalid for coordinate negative");}
    }

    public int getPositionX() {
        return PositionX;
    }

    public void setPositionX(int positionX) {
        this.PositionX = positionX;
    }

    public int getPositionY() {
        return PositionY;
    }

    public void setPositionY(int positionY) {
        this.PositionY = positionY;
    }
}