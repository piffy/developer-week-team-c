import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Game {
    public Maze maze;
    public Server server;
    public Drone drone;
    private ArrayList <Coordinate> path;
    private ArrayList <Coordinate> cross;
    private boolean deadEnd;
    private boolean foundCup;
    private ArrayList <Coordinate> visited;
    private String status;
    private int totalEnergy;
    private int energySpentSearching;
    private int energySpentTransporting;
    private int residualEnergy;

    public Game(String seed) throws IOException {
        this.server = new Server(seed);
        this.maze = new Maze(server.width, server.height);
        this.drone = new Drone(server.positionX, server.positionY);
        totalEnergy = server.energy;
        path = new ArrayList<>();
        cross = new ArrayList<>();
        deadEnd = false;
        visited = new ArrayList<>();
        foundCup = false;
        maze.setStartX(server.positionX);
        maze.setStartY(server.positionY);
    }

    public void move() throws IOException {
        int street = 0;
        if(deadEnd == false){
            server.look();
            drone.setPositionX(server.positionX);
            drone.setPositionY(server.positionY);
            for(int i=0; i<4; i++){
                if(server.neighbors[i] == 0 || server.neighbors[i] == 2 && (!(visited.contains(new Coordinate(drone.getPositionX(), drone.getPositionY()-1))) ||
                  !(visited.contains(new Coordinate(drone.getPositionX()+1, drone.getPositionY()))) ||
                  !(visited.contains(new Coordinate(drone.getPositionX(), drone.getPositionY()+1))) ||
                  !(visited.contains(new Coordinate(drone.getPositionX()-1, drone.getPositionY()))))) street += 1;
            }
            if((path.contains(new Coordinate(server.positionX, server.positionY-1)) ||
                visited.contains(new Coordinate(server.positionX, server.positionY-1)) ||
                server.neighbors[0] == 1 || server.neighbors[0] == -1) &&
               (path.contains(new Coordinate(server.positionX+1, server.positionY)) ||
                visited.contains(new Coordinate(server.positionX+1, server.positionY)) ||
                server.neighbors[1] == 1 || server.neighbors[1] == -1) &&
               (path.contains(new Coordinate(server.positionX, server.positionY+1)) ||
                visited.contains(new Coordinate(server.positionX, server.positionY+1)) ||
                server.neighbors[2] == 1 || server.neighbors[2] == -1) &&
               (path.contains(new Coordinate(server.positionX-1, server.positionY)) ||
                visited.contains(new Coordinate(server.positionX-1, server.positionY)) ||
                server.neighbors[3] == 1 || server.neighbors[3] == -1)) deadEnd = true;
            if(street >= 3 && deadEnd == false) {
                Coordinate c = new Coordinate(server.positionX, server.positionY);
                cross.add(c);
            }
        }

        if(deadEnd == true){
            goBackToLastCross();
        }
        else {
            Coordinate c = new Coordinate(server.positionX, server.positionY);
            if(!(path.contains(c))) path.add(c);
            if(server.neighbors[0] == 0) maze.setHallway(server.positionX, server.positionY-1);
            else if(server.neighbors[0] == 1) maze.setWall(server.positionX, server.positionY-1);
            else if(server.neighbors[0] == 2) maze.setStart(server.positionX, server.positionY-1);
            else if(server.neighbors[0] == 3) maze.setCup(server.positionX, server.positionY-1);

            if(server.neighbors[1] == 0) maze.setHallway(server.positionX+1, server.positionY);
            else if(server.neighbors[1] == 1) maze.setWall(server.positionX+1, server.positionY);
            else if(server.neighbors[1] == 2) maze.setStart(server.positionX+1, server.positionY);
            else if(server.neighbors[1] == 3) maze.setCup(server.positionX+1, server.positionY);

            if(server.neighbors[2] == 0) maze.setHallway(server.positionX, server.positionY+1);
            else if(server.neighbors[2] == 1) maze.setWall(server.positionX, server.positionY+1);
            else if(server.neighbors[2] == 2) maze.setStart(server.positionX, server.positionY+1);
            else if(server.neighbors[2] == 3) maze.setCup(server.positionX, server.positionY+1);

            if(server.neighbors[3] == 0) maze.setHallway(server.positionX-1, server.positionY);
            else if(server.neighbors[3] == 1) maze.setWall(server.positionX-1, server.positionY);
            else if(server.neighbors[3] == 2) maze.setStart(server.positionX-1, server.positionY);
            else if(server.neighbors[3] == 3) maze.setCup(server.positionX-1, server.positionY);

            if(foundCup == false){
                if(server.neighbors[0] == 3){
                    foundCup = true;
                    server.move(0);
                    server.load();
                    energySpentSearching = totalEnergy - server.energy;
                }
                else if(server.neighbors[1] == 3){
                    foundCup = true;
                    server.move(2);
                    server.load();
                    energySpentSearching = totalEnergy - server.energy;
                }
                else if(server.neighbors[2] == 3){
                    foundCup = true;
                    server.move(4);
                    server.load();
                    energySpentSearching = totalEnergy - server.energy;
                }
                else if(server.neighbors[3] == 3){
                    foundCup = true;
                    server.move(6);
                    server.load();
                    energySpentSearching = totalEnergy - server.energy;
                }
                else{
                    Random random = new Random();
                    int randomDirection = random.nextInt(4);

                    if(randomDirection == 0){
                        if(server.neighbors[0] == 0 && !(path.contains(new Coordinate(c.getX(), c.getY()-1))) && !(visited.contains(new Coordinate(c.getX(), c.getY()-1)))) server.move(0);
                    }
                    else if(randomDirection == 1){
                        if(server.neighbors[1] == 0 && !(path.contains(new Coordinate(c.getX()+1, c.getY()))) && !(visited.contains(new Coordinate(c.getX()+1, c.getY())))) server.move(2);
                    }
                    else if(randomDirection == 2){
                        if(server.neighbors[2] == 0 && !(path.contains(new Coordinate(c.getX(), c.getY()+1))) && !(visited.contains(new Coordinate(c.getX(), c.getY()+1)))) server.move(4);
                    }
                    else if(randomDirection == 3){
                        if(server.neighbors[3] == 0 && !(path.contains(new Coordinate(c.getX()-1, c.getY()))) && !(visited.contains(new Coordinate(c.getX()-1, c.getY())))) server.move(6);
                    }
                }
            }
            else{
                deadEnd = false;
                returnToStart();
            }
        }
    }

    public void goBackToLastCross() throws IOException {
        server.look();
        drone.setPositionX(server.positionX);
        drone.setPositionY(server.positionY);
        if(!(cross.isEmpty())){
            if(cross.get(cross.size()-1).getX() == drone.getPositionX() && cross.get(cross.size()-1).getY() == drone.getPositionY()){
                deadEnd = false;
                cross.remove(cross.size()-1);
            }
            else if(path.get(path.size()-1).getX() > drone.getPositionX()){
                Coordinate c = new Coordinate(drone.getPositionX(), drone.getPositionY());
                visited.add(c);
                if(!(path.isEmpty())) path.remove(path.size()-1);
                server.move(2);
            }
            else if(path.get(path.size()-1).getX() < drone.getPositionX()){
                Coordinate c = new Coordinate(drone.getPositionX(), drone.getPositionY());
                visited.add(c);
                if(!(path.isEmpty())) path.remove(path.size()-1);
                server.move(6);
            }
            else if(path.get(path.size()-1).getY() > drone.getPositionY()){
                Coordinate c = new Coordinate(drone.getPositionX(), drone.getPositionY());
                visited.add(c);
                if(!(path.isEmpty())) path.remove(path.size()-1);
                server.move(4);
            }
            else if(path.get(path.size()-1).getY() < drone.getPositionY()){
                Coordinate c = new Coordinate(drone.getPositionX(), drone.getPositionY());
                visited.add(c);
                if(!path.isEmpty()) path.remove(path.size()-1);
                server.move(0);
            }
            else if(maze.getStartX() > drone.getPositionX()){
                Coordinate c = new Coordinate(drone.getPositionX(), drone.getPositionY());
                visited.add(c);
                if(!(path.isEmpty())) path.remove(path.size()-1);
                server.move(2);
            }
            else if(maze.getStartX() < drone.getPositionX()){
                Coordinate c = new Coordinate(drone.getPositionX(), drone.getPositionY());
                visited.add(c);
                if(!(path.isEmpty())) path.remove(path.size()-1);
                server.move(6);
            }
            else if(maze.getStartY() < drone.getPositionY()){
                Coordinate c = new Coordinate(drone.getPositionX(), drone.getPositionY());
                visited.add(c);
                if(!(path.isEmpty())) path.remove(path.size()-1);
                server.move(0);
            }
        }
    }

    public void returnToStart() throws IOException {
        server.look();
        drone.setPositionX(server.positionX);
        drone.setPositionY(server.positionY);
        if(drone.getPositionX() == maze.getStartX() && drone.getPositionY() == maze.getStartY()){
            server.unload();
            residualEnergy = server.energy;
            status = server.status;
        }
        if((server.neighbors[0] == 0 || server.neighbors[0] == 2) && path.contains(new Coordinate(server.positionX, server.positionY-1))){
            server.move(0);
            path.remove(path.size()-1);
        }
        else if((server.neighbors[1] == 0 || server.neighbors[1] == 2) && path.contains(new Coordinate(server.positionX+1, server.positionY))){
            server.move(2);
            path.remove(path.size()-1);
        }
        else if((server.neighbors[2] == 0 || server.neighbors[2] == 2) && path.contains(new Coordinate(server.positionX, server.positionY+1))){
            server.move(4);
            path.remove(path.size()-1);
        }
        else if((server.neighbors[3] == 0 || server.neighbors[3] == 2) && path.contains(new Coordinate(server.positionX-1, server.positionY))){
            server.move(6);
            path.remove(path.size()-1);
        }
        energySpentTransporting = (totalEnergy - energySpentSearching) - server.energy;
    }

    public String getStatus(){
        return status;
    }

    public int getEnergySpentSearching(){
        return energySpentSearching;
    }

    public int getEnergySpentTransporting(){
        return energySpentTransporting;
    }
    public int getResidualEnergy(){
        return residualEnergy;
    }
}