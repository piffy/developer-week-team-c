import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

public class GridPanel extends JPanel {

    private int[][] maze;
    private int Initalbattery;
    private BufferedImage grass;
    private BufferedImage wall;
    private BufferedImage dro;
    private BufferedImage droC;
    private Game game;
    private BufferedImage darknessFilter;

    public GridPanel(Game game) {
        this.game = game;
        try {
            wall = ImageIO.read(getClass().getResourceAsStream("/wall.png"));
            grass = ImageIO.read(getClass().getResourceAsStream("/grass01.png"));
            dro = ImageIO.read(getClass().getResourceAsStream("/drone.png"));
            droC = ImageIO.read(getClass().getResourceAsStream("/droneCup.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setSize(700,700);
    }

    public void setMaze(int[][] m) {
        this.maze = m;
        repaint();
    }

    public void setBattery(int b) {
        this.Initalbattery = b;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //Creation of  the Maze
        if (maze != null) {
            int cellWidth = getWidth() / (maze[0].length + 1);
            int cellHeight = getHeight() / maze.length;

            for (int i = 0; i < maze.length; i++) {
                for (int j = 0; j < maze[i].length; j++) {
                    if (maze[i][j] == 1) {
                        g.drawImage(wall, j * cellWidth, i * cellHeight, cellWidth, cellHeight, null);
                    } else if (maze[i][j] == 0) {
                        g.drawImage(grass, j * cellWidth, i * cellHeight, cellWidth, cellHeight, null);
                    } else if (maze[i][j] == 2) {
                        g.setColor(Color.GREEN);
                        g.fillRect(j * cellWidth, i * cellHeight, cellWidth, cellHeight);
                    } else if (maze[i][j] == 3) {
                        g.setColor(Color.RED);
                        g.fillRect(j * cellWidth, i * cellHeight, cellWidth, cellHeight);
                    }
                }
            }

            //Creation of the Drone
            if (game.drone != null) {
                int droneX = game.drone.getPositionX() * cellWidth;
                int droneY = game.drone.getPositionY() * cellHeight;
                if (game.server.inventory!=null) {
                    g.drawImage(droC, droneX, droneY, cellWidth, cellHeight, null);
                } else {
                    g.drawImage(dro, droneX, droneY, cellWidth, cellHeight, null);
                }
            }

            // Draw the battery
            int batteryWidth = cellWidth * 2;
            int batteryHeight = cellHeight * maze.length;
            int batteryBarHeight = (int) ((double) game.server.energy / Initalbattery * batteryHeight);
            Color batteryColor;
            if (game.server.energy >= ((Initalbattery / 3) * 2)) {
                batteryColor = Color.GREEN;
            } else if (game.server.energy >= ((Initalbattery / 3) * 1)) {
                batteryColor = Color.ORANGE;
            } else {
                batteryColor = Color.RED;
            }
            g.setColor(batteryColor);
            g.fillRect(maze[0].length * cellWidth, batteryHeight - batteryBarHeight, batteryWidth, batteryBarHeight);

            //Draw border of Battery bar
            g.setColor(Color.BLACK);
            g.drawRect(maze[0].length * cellWidth, batteryHeight - batteryBarHeight, batteryWidth, batteryBarHeight);

            //Draw Battery Text
            Font font = new Font("Arial", Font.BOLD, 14);
            g.setFont(font);
            FontMetrics fontMetrics = g.getFontMetrics(font);
            int textX = maze[0].length * cellWidth + (batteryWidth) / 5;
            int textY = batteryHeight - batteryBarHeight + fontMetrics.getHeight();
            g.drawString(game.server.energy+"", textX, textY);

            // Add the illumination effect
            if (darknessFilter != null) {
                g.drawImage(darknessFilter, 0, 0, null);
            }
        }
    }

    // Method to apply "build" the illumination effect
    public void Light() {
        if (game.drone != null) {
            int droneX = game.drone.getPositionX() * (getWidth() / (maze[0].length + 1));
            int droneY = game.drone.getPositionY() * (getHeight() / maze.length);

            darknessFilter = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = (Graphics2D) darknessFilter.getGraphics();

            // Get the x and y of the drone position
            int centerX = droneX + (getWidth() / (maze[0].length + 1)) / 2;
            int centerY = droneY + (getHeight() / maze.length) / 2;

            int circleSize = 100;

            // Create a gradation effect
            Color color[] = new Color[30];
            float fraction[] = new float[30];

            for (int i = 0; i < 30; i++) {
                // Set a uniform transparency for each color
                color[i] = new Color(0, 0, 0, (float) i / 40);
                fraction[i] = (float) i / 30;
            }

            RadialGradientPaint gPaint = new RadialGradientPaint(centerX, centerY, circleSize, fraction, color);

            // Fill the maze area with illumination effect
            g2.setPaint(gPaint);
            g2.fillRect(0, 0, getWidth()-90, getHeight());

            g2.dispose();
        }
    }




}

